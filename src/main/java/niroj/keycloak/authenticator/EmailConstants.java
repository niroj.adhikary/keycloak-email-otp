package niroj.keycloak.authenticator;

import lombok.experimental.UtilityClass;

@UtilityClass
public class EmailConstants {
	public String CODE = "code";
	public String CODE_LENGTH = "length";
	public String CODE_TTL = "ttl";
	public String SENDER_ID = "senderId";
	public String SIMULATION_MODE = "simulation";
}